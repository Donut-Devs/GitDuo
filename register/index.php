<!DOCTYPE html>
<html lang="en">

<head>
    <title>Hypixelstats</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="https://jaywilliams.me/cdn-cgi/apps/head/HbOS5WhsGeLZnHpNOPMgg04KtYo.js"></script>
    <script type="text/javascript">
        //<![CDATA[
        try {
            if (!window.CloudFlare) {
                var CloudFlare = [{
                    verbose: 0,
                    p: 0,
                    byc: 0,
                    owlid: "cf",
                    bag2: 1,
                    mirage2: 0,
                    oracle: 0,
                    paths: {
                        cloudflare: "https://ajax.cloudflare.com/cdn-cgi/nexp/dok3v=9eecb7db59/",
                        "cloudflare-static": "https://ajax.cloudflare.com/cdn-cgi/scripts/c2b63e8a/"
                    },
                    atok: "bf773d0de5a128991be580dcc2eb817f",
                    petok: "948b882b77e31e174f4c85cc3062f0c42fea0c75-1506790220-1800",
                    rocket: "a",
                    zone: "jaywilliams.me"
                }];
                document.write('<script type="text/javascript" src="https://ajax.cloudflare.com/cdn-cgi/nexp/dok3v=c37cbdadf2/cloudflare.min.js"><' + '\/script>');
            }
        } catch (e) {};
        //]]>
    </script>
    <script type="text/rocketscript">if (typeof module === 'object') {window.module = module; module = undefined;}</script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <style>
        .form-signin {
            max-width: 330px;
            padding: 15px;
            margin: 0 auto;
        }
        
        .login-input {
            margin-bottom: -1px;
            border-bottom-left-radius: 0;
            border-bottom-right-radius: 0;
        }
        
        .login-input-pass {
            margin-bottom: 10px;
            border-top-left-radius: 0;
            border-top-right-radius: 0;
        }
        
        .body {
            .bg-color: #fff
        }
        
        .card {
            box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
            transition: 0.3s;
            border-radius: 5px;
            /* 5px rounded corners */
        }
        /* Add rounded corners to the top left and the top right corner of the image */
        
        .signup-input {
            margin-bottom: -1px;
            border-bottom-left-radius: 0;
            border-bottom-right-radius: 0;
        }
        
        .signup-input-confirm {
            margin-bottom: 10px;
            border-top-left-radius: 0;
            border-top-right-radius: 0;
        }
        
        .form-signin .form-control {
            position: relative;
            font-size: 16px;
            height: auto;
            padding: 10px;
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            box-sizing: border-box;
        }
        
        .btn-center {
            width: 50%;
            text-align: center;
            margin: inherit;
        }
        
        .social-login-btn {
            margin: 5px;
            width: 20%;
            font-size: 250%;
            padding: 0;
        }
        
        .social-login-more {
            width: 45%;
        }
        
        .social-google:hover {
            background-color: #ddd;
            border-color: #aaa;
        }
        
        .social-google {
            background-color: #eee;
            border-color: #fff;
        }
        * {
    box-sizing: border-box;
}

.columns {
    float: left;
    width: 33.3%;
    padding: 8px;
}

.price {
    list-style-type: none;
    border: 1px solid #eee;
    margin: 0;
    padding: 0;
    -webkit-transition: 0.3s;
    transition: 0.3s;
}

.price:hover {
    box-shadow: 0 8px 12px 0 rgba(0,0,0,0.2)
}

.price .header {
    background-color: #111;
    color: white;
    font-size: 25px;
}

.price li {
    border-bottom: 1px solid #eee;
    padding: 20px;
    text-align: center;
}

.price .grey {
    background-color: #eee;
    font-size: 20px;
}

.button {
    background-color: #4CAF50;
    border: none;
    color: white;
    padding: 10px 25px;
    text-align: center;
    text-decoration: none;
    font-size: 18px;
}

@media only screen and (max-width: 600px) {
    .columns {
        width: 100%;
    }
}
    </style>
</head>

<body>
    <div class="container">
        <div class="container text-center">
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <div class="card">
                <div class="container">
                    <h2 style="text-align:center">Register</h2>
                    <form class="form-signin" action"http://hypixelstats.000webhostapp.com" method="post">
                        <input type="text" class="form-control login-input" name="email" placeholder="Email" />
                        <br>
                        <input type="text" class="form-control login-input" name="name" placeholder="Username" />
                        <br>
                        <input type="text" class="form-control pass-input" name="pass" placeholder="Password" />
                        <br>
                        <button class="btn btn-lg btn-primary btn-block btn-center" type="submit">Register</button>
                        <br>
                        <p stlye="text-align:center"><a href="http://hypixelstats.000webhostapp.com">Alrady have an account?</a></p>
                        <br><br>
                    </form>
                    <br><br>
                    </div>
                </div>
            </div>
            <br><br>
        </div>
    </div>
</body>
<script data-rocketsrc=" https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js " type="text/rocketscript "></script>
<script data-rocketsrc="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js " type="text/rocketscript "></script>
<script type="text/rocketscript ">if (window.module) module = window.module;</script>

</html>