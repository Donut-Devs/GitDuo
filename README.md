# GitDuo

We were bored so we experimented with apis to try something with git! Work in progress - not meant to be a replacement *(Yet)*

## Built With

* [HTML5](https://html.spec.whatwg.org/) - Key to getting this up and running
* [PHP](http://php.net/) - Key as well

## Author

* **[Patchy319](https://gitlab.com/Patchy319/)**
* [Full List](https://gitlab.com/Donut-Devs/GitDuo/graphs/master)


### Acknowledgments

* None for this one!
